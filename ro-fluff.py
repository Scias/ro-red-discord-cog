import discord
import json
import os
from .utils import checks
from discord.ext import commands
from random import randint
from random import choice
from random import sample

# tiers 0 1 2 3 4 5 6 7

class Item:
    def __init__(self):
        self.types=("weapon","armor")
        self.rarity={
            "rock" : 0,
            "defective" : 5,
            "normal" : 65,
            "special" : 120,
            "magical" : 150,
            "epic" : 185,
            "legendary" : 190,
            "boss" : 198
            }
        self.legend_bonuses={
            "Movement Speed" : 5,
            "Healing Bonus" : 5,
            "Class Attribute" : 5,
            "Regenerate Health" : 5,
            "Main Attribute" : 5,
            "Resist Immobilize" : 5,
            "Resist Knockdown" : 5
            }
        self.armor_bonuses={
            "Armour Bonus" : 5,
            "Resist Physical Damage" : 5,
            "Resist Slashing Damage" : 5,
            "Resist Piercing Damage" : 5,
            "Resist Blunt Damage" : 5,
            "Resist Magical Damage" : 5,
            "Resist Fire Damage" : 5,
            "Resist Ice Damage" : 5,
            "Resist Lightning Damage" : 5
            }
        self.weapon_bonuses={
            "Slashing Damage" : 25,
            "Blunt Damage" : 25,
            "Piercing Damage" : 25,
            "Fire Damage" : 25,
            "Ice Damage" : 25,
            "Lightning Damage" : 25,
            "Attack Range" : 7,
            "Critical Chance" : 50
            }
        self.bonuses={
            "Strenght" : 7,
            "Dexterity" : 7,
            "Intelligence" : 7,
            "Constitution" : 7,
            "Cast Speed" : 7,
            "Concentration" : 7,
            "Attack Speed" : 7,
            "Mana" : 180,
            "Health" : 180
            }
        self.crap=("a rock","worthless loot","Punzaz's bow","Soras' trident","Cupid's bow","a level 30 item","a week horse","blood arrows","Satarco Sword","a failed crafting item","a golem rock","an amber rock")
        self.bossdrops=("Spiritual Essence Amulet","Ring of Undead Touch","Ring of Mighty Energy")
        self.factor = 1
        
    def generate(self, tier=None):
        chance = randint(0,200)
        bonuslist = self.bonuses
        bonus_nbr = 0
        chosen_bonuses = chosen_values = []
        
        if not tier or tier not in self.rarity.keys():
            for key, value in self.rarity.items():
                if chance < value:
                    self.tier = key
                    break
            else:
                self.tier = None
                return
        else:
            self.tier = tier
            
        if self.tier == "rock":
            chance = randint(0,10)
            if chance < 3:
                self.kind = "magna"
                self.nb = randint(1,5)
            else:
                self.kind = choice(self.crap)
            return
        elif self.tier == "boss":
            self.kind = choice(self.bossdrops)
            return
            
        self.kind = choice(self.types)
        
        if self.tier == "normal":
            return (self.kind,self.tier)
        elif self.tier == "special" or self.tier == "defective":
            bonus_nbr = 1
        elif self.tier == "magical":
            bonus_nbr = 2
        elif self.tier == "epic" or self.tier == "legendary":
            bonus_nbr = 3
            
        if self.kind == "armor":
            bonuslist.update(self.armor_bonuses)
        elif self.kind == "weapon":
            bonuslist.update(self.weapon_bonuses)
            
        chosen_bonuses = sample(list(bonuslist), bonus_nbr)
        
        if self.tier == "legendary":
            extra_bonus = choice(list(self.legend_bonuses))
            chosen_bonuses.append(extra_bonus)
            bonuslist.update(self.legend_bonuses)
        
        for bonus in chosen_bonuses:
            max_value = bonuslist[bonus]
            chance = randint(1,max_value)
            chosen_values.append(chance)
            
        self.stats = chosen_bonuses
        self.values = chosen_values


class Fluff:
    """RO Fluff"""
    
    def __init__(self, bot):
        self.bot = bot
        self.allowedcheaters=("281067341566050304")
    
    @commands.command(pass_context=True)
    @commands.cooldown(10, 60, commands.BucketType.user)
    async def drop(self, ctx, tier=None):
        """Gives you a random item...or not!"""
        
        if tier and ctx.message.author.id not in self.allowedcheaters:
            #await self.bot.say("{}: You aren't allowed to cheat!".format(ctx.message.author.mention))
            tier=None
        
        if not tier:
            chance = randint(0,100)
        else:
            chance = 10
        
        msg = "You get "
        
        if chance <= 50:
            drop = Item()
            drop.generate(tier)
            if drop.tier == "rock":
                if drop.kind == "magna":
                    msg += "{} Magnanite(s)!".format(drop.nb)
                else:
                    msg += "{}!".format(drop.kind)
            elif drop.tier == "boss":
                msg += "{}!!!".format(drop.kind)
            elif drop.tier == "normal":
                msg += "a {} {}!".format(drop.tier,drop.kind)
            else:
                if drop.tier == "epic":
                    msg += "an "
                else:
                    msg += "a "
                msg += "{} {} with:\n".format(drop.tier,drop.kind)
                for bonus, value in zip(drop.stats, drop.values):
                    if drop.tier == "defective":
                        msg += "• {} -{}\n".format(bonus,value)
                    else:
                        msg += "• {} +{}\n".format(bonus,value)
        elif chance > 50 and chance <= 80:
            msg += "{} gold!".format(randint(1,1000))
        elif chance > 80 and chance <= 97:
            msg += "nothing!"
        else:
            msg += "nothing! You lose! Good day, sir!"
        
        await self.bot.say("{}: {}".format(ctx.message.author.mention, msg))
        
    @commands.command()
    @checks.is_owner()
    async def dropset(self, factor):
        self.factor = int(factor)
        await self.bot.say("Done")

def setup(bot):
    n = Fluff(bot)
#    bot.add_listener(n.watch, "on_message")
    bot.add_cog(n)
