I stay with you all the day but not at night. What am I?`shadow
How many days are there in 4 years?`1461
I gurgle but never speak, run but never walk, have a bed but never sleep. What am I?`river
What do you get when you mix a snowman and a wolf?`Frostbite
What has a bed but does not sleep, and can run but cannot walk?`river
I am not alive but I can grow. I do not have lungs but I need air to survive. What am I?`Fire
There was a girl half my age when I was 12, now I am 64, how old is she?`58
What type of shoes do spies wear?`Sneakers
You can drop me from the tallest building and I’ll be fine, but if you drop me in water I die. What am I?`Paper
When does yesterday come after today?`In the dictionary`dictionary
You bury me when I’m alive and dig me up when I’m dead.  What am I?`plant
I have an eye but cannot see. I am fast but I have no limbs. What am I?`hurricane
What spends the day at the window, goes to the table for meals and hides at night?`fly
I can be written, I can be spoken, I can be exposed, I can be broken. What am I?`News
Everyone has me except for a few. You may not be able to identify me but I can always identify you. What am I?`fingerprint
Sometimes I walk in front of you.  Sometimes I walk behind you.  It is only in the dark that I ever leave you.  What am I?`shadow
When I take five and add six, I get eleven, but when I take six and add seven, I get one. What am I?`clock
Most people need it,  some ask for it, some give it, but almost nobody takes it. What is it?`Advice
I turn around once. What is out will not get in. I turn around again. What is in will not get out. What am I?`key
What goes into the water black and comes out red?`Lobster
It weighs next to nothing but no one can hold it for long.  What is it?`breath
What has 13 hearts, but no other organs?`deck of cards`card deck
What never asks questions but is often answered?`doorbell
What comes down but never goes up?`Rain
You want to purchase 3-cent stamps. You’re at the post office and waiting in line for your turn. How many 3-cent stamps you will get in a dozen?`12
What kind of vegetable is unpopular on board ships?`leek
I always follow my brother but you cannot see me, only him.  You cannot hear him but you can hear me.  What are we?`Thunder and Lightning`Thunder lightning
If it’s information you seek, come and see me. If it’s pairs of letters you need, I have consecutively three. What am I?`bookkeeper
People always ask for me even though they don’t like to face me.  What am I?`truth
Say the following: roast, boast, coast, post.  What do you put in a toaster?`Bread
Every night I’m told what to do and each morning I do what I’m told, but I still do not escape your scold.  What am I?`alarm clock
You walk into a room and see a rabbit holding a carrot, a pig eating slop, and a chimpanzee holding a banana. Which is the smartest the smartest animal in the room?`You
I go up and I go down, towards the sky and the ground. I’m present and past tense too, Let’s go for a ride, me and you.  What am I?`SeeSaw
Which word does not belong in the following list: Stop prop crop shop drop or flop?`or
If your aunt’s brother is not your uncle, what relation is he to you?`father
People buy me to eat but they never eat me.  What am I?`Plates`cutlery
What word looks the same upside down and backwards?`SWIMS
Can you name three consecutive days without using Sunday, Wednesday and Friday?`Yesterday today tomorrow
A is the father of B, but B is not the son of A.  How is that possible?`B is daughter of A`
It is said among my people that some things are improved by death. Tell me, what stinks while living but in death smells good?`pig
What kind of coat is best put on wet?`coat of paint
Whoever makes me, tells me not. Whoever takes me, knows me not. Whoever knows me, wants me not.  What am I?`Counterfeit money
What never gets any wetter no matter how hard it rains?`Water
What flies when it’s born, lies when it’s alive, and runs when it’s dead?`Snowflake
I am a protector. I sit on a bridge. One person can see right through me, while others wonder what I hide. What am I?`Sunglasses
I have many keys but I cannot open a single lock.  What am I?`Piano
On which day of the year do fewest people die?`February 29`29 February`29/02`29/2
What is the only word that is spelled wrong in the dictionary?`Wrong
Two bodies have I, though both joined in one. The more I stand still the faster I run. What am I?`hourglass
What can you easily break but never touch?`promise
I contain five little items of an everyday sort. You can find all five in a tennis court. What am I?`Vowels
I have keys but no locks. I have space but no room. You can enter but cant go outside.  What am I?`Keyboard
What gets wetter the longer it is left out in the sun?`Ice
What is broken if you can’t break it?`Piñata`pinata
I am first on earth, second in heaven. I appear twice in a week, never in a month, but once in a year What am I?`The letter “E”`E
What do you get when you cross an elephant with a rhino?`Elephino
I can tear down mountains, or build them up. I can blind a man, or enable him to see. What am I?`Sand
What can always be measured but never can be seen?`Time
What is the next number in the following series: 100 365 24 60?`60
Divide 20 by half and add 30, what do you get?`70
What side of a cat has the most fur?`outside
What three letters change a girl into a woman?`Age
What word when read from left to right is a ruler but when read right to left is a servant?`Dog
Be quick and say the first thing that comes to your head. What do cows drink?`Water
Who was the president of the USA before John F. Kennedy was assassinated?`John F. Kennedy`Kennedy
What is the Easter bunny’s favorite kind of music?`Hip hop
What question can you ask where you can get different answers every time but all the answers being correct?`What time is it?
You walk a mile south, a mile east and then a mile north. You end up in exact same spot you started. Where are you?`North Pole
Everyone is attracted to me and everybody falls for me. What am I?`Gravity
What must you give before you can keep it?`Your word`word
What do people spend a lot of money on every year but never want to use?`Insurance
Which berry makes a harsh noise?`Raspberry
I am the son of your grandmother and grandfather but not your uncle. Who am I?`Your Dad`Dad
What is something you can give away as many times you want without ever losing it?`Your name`name
What do you get when you mix lemons with gun powder?`Lemonades
What man can shave 10 times a day and still have a beard?`Barber
What is the next letter in this sequence: JFMAMJJASON_ ?`D
Which word contains all 26 letters?`Alphabet
If you tell me the truth I will kill you with my sword, if you tell me a lie I will kill you with my spell. What must you say to survive?`You will kill me with your spell
What three numbers result in the same answer whether they are added together or multiplied?`1 2 3
When the day after tomorrow is yesterday, today will be as far from Wednesday as today was from Wednesday when the day before yesterday was tomorrow. What is the day after this day?`Thursday
It is there from the very start and will be there until the end. To end you must cross over and you must pass through it to begin.`Starting line
What two letters in the alphabet say goodbye?`CU
What starts with a T ends with a T and is full of T?`Teapot
What turns everything around but does not move?`A mirror
I touch the Earth, I touch the sky, but if I touch you, you’ll likely die. What am I?`Lightning
What is big and yellow (in the US) and comes in the morning to brighten mom’s day?`School bus
What do you throw out when you want to use it, but take in when you don’t want to use it?`Anchor
Take away my first letter and I sound the same. Take away my last letter, I still sound the same. Take away my letter in the middle, I will still sound the same. I am a 5 letter word. What am I?`Empty
You use a knife to slice my head and weep beside me when I am dead. What am I?`Onion
I am always in front of you but yet I am never here. What am I?`Future
I know a word of letters three. Add two and fewer there will be. What is the word?`Few
Where does the Easter bunny like to eat breakfast?`IHOP
What do frogs order when they go to a restaurant?`French Flies
What does everyone know how to open but not how to close?`Egg
What has wheels and flies but is not an aircraft?`Garbage truck
You use it between your head and your toes, the more it works the thinner it grows. What is it?`Bar of soap`soap
What is put on a table, cut, but never eaten?`Deck of cards`card deck
I run but cannot walk, sometimes sing but never talk. What am I?`clock
What can you hold but not touch and release without throwing?`breath
What jumps when it walks but sits when it stands?`kangaroo
What can fly without wings?`Time
My seas have no water, my mountains have no rock, and my land has no grass. what am I?`Map
What has a bottom at the top?`Leg
I start with “e” end with “e”. I have whole countries inside me. What am I?`Europe
I am needed in life but not in death.  You can’t have fun without me.  What am I?`The letter F
Sometimes I am light, sometimes I am dark.  Most people like me.  What am I?`Chocolate
A barrel of water weighs 75 pounds.  What must you add to it to make it weigh 60 pounds?`Holes
What is the sun’s favorite day of the week?`Sunday
What has thousands of ribs and two backbones?`Train tracks
I have two hands, but cannot hold. I have no mouth, but the unknown can still be told. What am I?`Clock
What color can you eat?`Orange
What has no lid, hinges or key, yet golden treasure can be found inside?`Egg
What is in seasons, seconds, centuries and minutes but not in decades, years or days?`The Letter “n”
What has a tongue but does not eat or speak?`Shoe
Where do polar bears go to vote?`The North Poll
I never speak unless spoken to, many have heard me but none have seen me. What am I?`Echo
What can fill an entire room without taking up any space?`Light
A square house is being circled by a large bear.  All of the walls of the house face South.  What color is the bear?`White because it is a polar bear`White
What is weightless but takes two people to hold it?`Friendship
How many animals of each species did Moses take on the ark with him?`None`0
If you look in water you might see me yet I never get wet.  What am I?`Reflection
You can touch me but you probably wouldn’t want to.  I can see you but you can’t see me.  What am I?`Your eyes`Eye
At night they come without being fetched. By day they are lost without being stolen. What are they?`Stars
I am the only organ in the human body that gave itself it’s own name. What am I?`The brain`Brain
Which word in the dictionary is spelled incorrectly?`Incorrectly
I no longer have eyes, but once I did see. Once I had thoughts, but now I’m white and empty. What am I?`Skull
You answer me, but I never ask you a question. What am I?`Telephone
Walk on the living, they don’t even mumble. Walk on the dead, they mutter and grumble. What are they?`Leaves
What is it called when Batman skips church?`Christian Bale
Forwards it’s heavy, but backwards it’s not.`Ton
Say my name and I am no more.`Silence
What can be larger than you without weighing anything?`Your shadow`shadow
It belongs to you but you don’t use it.  It does not belong to other people but they use it.  What is it?`Your phone number`phone number
What building has the most stories?`The library`library
What is something you will never see again?`Yesterday
If you jump off a two story building where to you land?`In the hospital
What can you hold in your right hand but never in your left hand?`Your left hand`left hand
What do you call a boomerang that doesn’t work?`A stick`stick
What English word sounds the same even after you take away four of its five letters?`Queue
You can easily touch me but not see me.  You can throw me out but never away. What am I?`Your back`back
If lightning strikes an orchestra who is the one most likely to get hit?`The conductor`conductor
You can’t keep this without giving it first?`promise
What has no beginning, end or middle?`A doughnut`doughnut`donut
What was the first man made invention that could see through a wall?`A window`window
How many eggs can you put into an empty basket?`One, then it will no longer be empty`One`1
What is black when you buy it, red when you use it, and gray when you throw it away?`Charcoal
What has a head but never weeps, has a bed but never sleeps, can run but never walks, and has banks but no money?`A river`river
Thirty-two white horses on a red hill. When you talk, they move. When you’re silent, they stand still. They can be lost, but replaced only once.`Teeth
Without me you will surely die.  Too much of me and like a pig you may lie.  What am I?`Food
Every night and every morning I am told what to do and I do it, yet everyone is still mad at me? What am I?`An alarm clock`alarm clock
What has four fingers and a thumb but is not alive?`A glove`glove
You can always find me in the past. I am created in the present, but the future can not change me.  What am I?`History
What goes up and down but never moves?`Stairs
What do you find in the middle of Paris?`The letter “R”`R
What’s in a man’s pants that you won’t find in a girl’s dress?`Pockets
What has six faces, but never wears makeup? It also has twenty-one eyes, but cannot see.`A die`die
What has been around for millions of years but is never more than a month old?`The moon`moon
If 3 salesmen can sell three cars in seven minutes, how many cars can six salesmen sell in seventy minutes?`60
What 3 digit number will give you the same answer whether you subtract 5 or divide by 5?`6.25
Joe has been hired to paint the numbers 1 through 100 on 100 apartments. How many times will he paint the number 8?`Twenty`20
What has feet and legs, but nothing else?`Pantyhose
What is harder to catch the faster you run?`Your breath`breath
What coin doubles in value when half is deducted?`Half dollar
Before Mt. Everest was discovered, what was the highest mountain in the world?`Mt. Everest
What has no beginning, end, or middle?`A donut`donut
Mary’s mother had three duaghters. The first was named April and the second was named May. What was the name of the third daughter?`Mary
Mr. Smith has 4 daughters. Each of his daughters has a brother.  How many children does Mr. Smith have?`Five`5
What connects two people but only touches one?`A wedding ring`wedding ring
What always goes to bed with its shoes on?`A horse`horse
What is the center of gravity?`V
What man has married many women but has always been single?`A priest`priest
What always murmurs but never talks. Always runs but never walks. Has a bed but never sleeps. Has a mouth but never speaks.`A river`river
What can one not keep, two hold, and three destroy?`A secret`secret
Who gets paid when they drive away their customers?`Taxi driver
What table you can eat?`Vegetable
What six letter word when you take away one letter leaves twelve?`Dozens
My age today is three times what it will be three years from now minus three times what my age was three years ago. How old am I?`18
The rungs of a 10 foot ladder attached to a ship are 1 foot apart. If the water is rising at the rate of two foot an hour, how long will it take until the water covers over the ladder?`Never, the ladder rises with the ship`Never
How much dirt is there in a hole that is 3 ft deep, and 9 inches in diameter?`None, it is a hole`None
What can go up a chimney down but can’t go down a chimney up?`An umbrella`umbrella
Imagine you are in a room with no doors or windows, how do you get out?`Stop imagining
What seven letter word has hundreds of letters in it?`Mailbox
A doctor gives you 5 pills and tells you to take one every 30 minutes.  How long will it take you to finish the pills if you follow the doctors orders?`2 hours
You can hold it without seeing or touching it. What is that?`Your breath
What can you add to a wooden box to make it lighter?`Holes
You have a dime and a dollar. You buy a dog and a collar.  The dog costs one dollar more than the collar.  How much is the collar?`5 cents
I am always on my way, but never arrive today. What am I?`Tomorrow
I am not alive but I can die.  What am I?`A battery`battery
I don’t have eyes, ears, nose or a tongue, but I can see, smell, hear and taste everything. What am I?`A brain`brain
What is the most slippery country in the world?`Greece
Which month has 28 days?`All of them`All
What gets whiter the dirtier it gets?`chalkboard
A butcher is 6 feet tall and wears size 12 shoes.  What does he weigh?`Meat
What is full of holes but can still hold water?`sponge
What’s a lifeguard’s favorite game?`Pool
The person who makes it, has no need of it. The person who buys it, has no use for it. The person who uses it can neither see nor feel it. What is it?`coffin
Where were English kings usually crowned?`head
A sundial is the timepiece with the least amount of moving parts.  What is the timepiece with the greatest amount of moving parts?`hourglass
What dress does everyone have, but no one wears?`address
Which side of a turkey has the most feathers?`outside
What do you answer that never asks a question?`telephone
What eight letter word contains all of the twenty six letters?`Alphabet
If you drop a yellow hat in the Red Sea, what does it become?`Wet
A cyclist in a cross-country race, just before the crossing finish line overtakes the person in second place. What place did the cyclist finish in?`Second
The more I appear The less you see. So riddle me this: What could I be?`Darkness
Why did the hamburger go to the gym?`It wanted better buns
I am easy to get into but hard to get out of.  What am I?`Trouble
What belongs to you but other people use it more than you do?`name
What has a neck but no head?`bottle
What instrument can you hear but never see?`voice
The more it dries, the wetter it becomes. What is it?`towel
How many of each animal did Moses take on the ark with him?`None
What do you call a rabbit with fleas?`Bugs Bunny
What can be broke without touching it?`promise
What goes up and never comes back down?`Age
What has to be broken before you can use it?`Egg
What word is always pronounced wrong?`Wrong
Which weighs more, a pound of feathers or a pound of bricks?`Neither`They weigh the same
What has one eye but cannot see?`needle
What kind of tree can you carry in your hand?`palm
What has 3 feet but cannot walk?`yardstick
What has hands but cannot clap?`clock
What has 4 eyes but can’t see?`Mississippi
What’s orange and sounds like a parrot?`carrot
What occurs once in a minute, twice in a moment and never in one thousand years?`m
What kind of room has no doors or windows?`mushroom
What goes up when rain comes down?`Umbrellas
What loses its head in the morning and gets it back at night?`pillow
What kind of dog keeps the best time?`Watchdog
What do you call bodyguards in front of a Samsung store?`Guardians of the Galaxy
A farmer has 10 cows, all but six die, how many are left?`Six`6
What number do you get when you multiply all of the numbers on a telephone’s dial pad?`Zero
What disappears the instant you say its name?`Silence
You throw away the outside then eat the inside.  Then you throw away the outside and eat the inside.  What is it?`Ear of corn
What asks but never answers?`Owl
I am tall when young but short when old? What am I?`Candle
What is brown and sticky?`Stick
What can you catch but can not throw?`Cold
What three numbers give the same answer when multiplied together or added together?`1 2 3
I travel all over the world, but always stay in my corner. What am I?`Stamp
What has a foot but no legs?`Snail
Poor people have it. Rich people need it. If you eat it you die. What is it?`Nothing
What has a head and a tail, but no body?`Coin
What two things can you never eat for breakfast?`Lunch & Dinner
The more you take, the more you leave behind. What are they?`Footsteps
What tastes better than it smells?`Tongue
What word becomes shorter when you add two letters to it?` Short
If I drink, I die. If i eat, I am fine. What am I?`Fire
