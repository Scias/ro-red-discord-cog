import discord
import os
import asyncio
from .utils import checks
from discord.ext import commands
from datetime import timedelta, datetime
from yaml import load, load_all, dump
from .utils.chat_formatting import box
from time import sleep

WORLDS = ["haven","ra"]
BOSSES = ["Daen Rha","Evendim","Thorkul"]
REALMS = ["Alsius","Ignis","Syrtis"]

FORTS = {}
FORTS["Alsius"] = ["Imperia","Aggersborg","Trelleborg","Alsius Great Wall"]
FORTS["Ignis"] = ["Menirah","Samal","Shaanarid","Ignis Great Wall"]
FORTS["Syrtis"] = ["Algaros","Herbred","Eferias","Syrtis Great Wall"]

WARSTATUS_PATH = "data/warstatus"

ROTHUMB = "https://42cdn.com/b2api/v1/b2_download_file_by_id?fileId=4_z6177fca890d8c37157cc0814_f108ef9bf69c8ccf1_d20171222_m174056_c001_v0001037_t0005"

def prettytime(delta):
    msg = str()
    
    hours = delta.seconds // 3600
    minutes = (delta.seconds // 60) % 60
    
    if delta.days > 0:
        if delta.days == 1: msg += f"1 day"
        else: msg += f"{delta.days} days"
    
    if hours > 0:
        if delta.days > 0:
            if minutes > 0: msg += ", "
            else: msg += " and "
        if hours == 1: msg += f"1 hour"
        else: msg += f"{hours} hours"
        
    if minutes > 0:
        if delta.days > 0 or hours > 0: msg += " and "
        if minutes == 1: msg += f"1 minute"
        else: msg += f"{minutes} minutes"
    return(msg)

class Warstatus:
    
    def __init__(self, bot):
        self.bot = bot
        self.folder = WARSTATUS_PATH
        
        self.servers = []
        
        with open(f"{self.folder}/servers.yaml", "r") as file:
            for server in load_all(file):
                self.servers.append(server)
        
    def _checknload(self, what=None, world=None):
        if what == "boss":
            target = ["boss.yaml"]
        elif world:
            target = [f"{world}.yaml"]
        else:
            target = [f"{w}.yaml" for w in WORLDS]
            
        try:
            data = []
            for t in target:
                with open(f"{self.folder}/{t}", "r") as file:
                    data.append(load(file))
        except:
            return(None)
        return(data)
            
    @commands.command(pass_context=True)
    async def action(self, ctx, world=None):
        '''Displays if anything is going on in the specified Regnum world, or for all servers if none specified.'''
        if world and world in WORLDS:
            data = self._checknload(world=world)
        else:
            data = self._checknload()
            
        if ctx.message.server is not None:
            servid = ctx.message.server.id
        else:
            servid = None
        
        for d in data:
            msg = str()
            nothing = True
            world = d["world"]
            for realm in REALMS:
                extforts = d[realm]["extforts"]
                if d[realm]["vulnerable"]:
                    if nothing: nothing = False
                    if servid:
                        for server in self.servers:
                            if servid == str(server["id"]):
                                msg += f'{server["emojis"]["flag"][realm]} '
                                break
                    msg += f"**{realm} is vulnerable!** :warning:"
                    msg += "\n"
                if extforts:
                    if nothing: nothing = False
                    for extfort in extforts:
                        fort, timestamp = extfort
                        
                        for fortrealm in REALMS:
                            if any(fort in f for f in FORTS[fortrealm]):
                                break
                        
                        if "Great Wall" not in fort:
                            line = f"**{realm}** captured **{fort}**"
                        else:
                            line = f"**{realm} is invading {fortrealm}**"
                        
                        if servid:
                            for server in self.servers:
                                if servid == str(server["id"]):
                                    if "Great Wall" not in fort:
                                        msg += f'{server["emojis"]["flag"][realm]} {line} {server["emojis"]["fort"][fortrealm]}'
                                    else:
                                        msg += f'{server["emojis"]["flag"][realm]} {line} {server["emojis"]["flag"][fortrealm]}'
                                    break
                            else:
                                msg += line
                        else:
                            msg += line
                        
                        if timestamp:
                            now = datetime.utcnow()
                            delta = now - datetime.fromtimestamp(timestamp)
                            msg += f" - *{prettytime(delta)} ago*"
                        msg += "\n"
                            
            if nothing:
                msg += f"All forts currently belong to their respective realms.\n" 
                
            msg += "\n"
            
            embed = discord.Embed(description=msg)
            embed.set_author(name=world.capitalize(), icon_url=ROTHUMB)
            
            await self.bot.say(embed=embed)
                
                
    
    @commands.command(aliases=["ws"], pass_context=True)
    async def warstatus(self, world):
        pass
        
#    @commands.command()
#    async def romap(self, world):
#        '''Displays the map for the specified world'''
#        if world in WORLDS:
#            await self.bot.say(f"https://www.championsofregnum.com/ranking/data/{world}/gen_map.jpg")
#        else:
#            await self.bot.say("Unknown world")
    
    @commands.command(aliases=["boss"], pass_context=True)
    async def bosses(self, ctx):
        '''Displays boss respawn dates and the remaining time for them'''
        data = self._checknload("boss")
        if not data:
            await self.bot.say("Error: Couldn't get boss times!")
            return
        
        bosstimes = data[0]
        
        if ctx.message.server is not None:
            servid = ctx.message.server.id
        else:
            servid = None
    
        cur_date = datetime.utcnow()
        msg = str()
        
        for boss in BOSSES:
            respawn = datetime.strptime(bosstimes[boss], "%d/%m/%Y %H:%M")
            delta = respawn - cur_date
            
            if servid:
                for server in self.servers:
                    if servid == str(server["id"]):
                        msg += server["emojis"]["boss"][boss]
                        break
                
            msg += f"**{boss}** : {respawn.strftime('%A **%d/%m** at **%H:%M** (UTC)')} - *In "
            msg += prettytime(delta)
            msg += "*\n"
        
        await self.bot.say(msg)
        
def check_folder():
    """Create folder/files if they don't exist"""
    if not os.path.exists(WARSTATUS_PATH):
        print(f"Creating {WARSTATUS_PATH} folder...")
        os.makedirs(WARSTATUS_PATH)

def setup(bot):
    check_folder()
    n = Warstatus(bot)
    bot.add_cog(n)
