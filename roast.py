import discord
from .utils import checks
from discord.ext import commands

class Roast:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def burn(self):
        await self.bot.say("https://memegenerator.net/img/instances/250x250/24579096/apply-cold-water-to-burned-area.jpg")

def setup(bot):
    n = Roast(bot)
    bot.add_cog(n)
