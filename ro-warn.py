import discord
import json
import os
import asyncio
from .utils.dataIO import dataIO
from .utils import checks
from discord.ext import commands

class Warnme:
    """Warne stuff"""
    
    def __init__(self, bot):
        self.bot = bot
        self.folder = "data/regnum"
        self.worlds = ("haven","ra")
        self.events = ("invasion","wish","boss","tdm")
        self.realms = ("alsius","ignis","syrtis")
        self.bosses = ("daen rha","evendim","thorkul")
        self.output_channel = None
        self.warstatus_bot_ids = {
            "346859119686189066" : "haven",
            "347851686255001601" : "ra",
            "371832962070609922" : "wishpin",
            "385559470991998987" : "wishpin"
                }
        self.loop = asyncio.get_event_loop()
        self.wish_triggers = {
            "<:alsius:346709384757510145> <:gemsyrtis:348134331832729603><:gemalsius:348134331589591042><:gemignis:348134331912421396><:gemalsius:348134331589591042><:gemignis:348134331912421396><:gemsyrtis:348134331832729603>" : "alsius",
            "<:ignis:346709083648294914> <:gemsyrtis:348134331832729603><:gemalsius:348134331589591042><:gemignis:348134331912421396><:gemalsius:348134331589591042><:gemignis:348134331912421396><:gemsyrtis:348134331832729603>" : "ignis",
            "<:syrtis:346709406035214347> <:gemsyrtis:348134331832729603><:gemalsius:348134331589591042><:gemignis:348134331912421396><:gemalsius:348134331589591042><:gemignis:348134331912421396><:gemsyrtis:348134331832729603>" : "syrtis"
                }
        self.wish_timer = {
            "haven" : None,
            "ra" : None
            }
        
    
    def filecheck(self):
        """Create folder/files if they don't exist"""
        if not os.path.exists(self.folder):
            print("Creating {} folder...".format(self.folder))
            os.makedirs(self.folder)
            
        for world in self.worlds:
            filename = self.folder + "/warnme-" + world + ".json"
            if not dataIO.is_valid_json(filename):
                skel = {}
                print("Creating skeleton {}...".format(filename))
                for key in self.events:
                    if key == "invasion" or key == "wish":
                        skel[key] = { "alsius" : [], "ignis" : [], "syrtis" : [] }
                    else:
                        skel[key] = []
                dataIO.save_json(filename, skel)
    
    
    def update(self, event, p_mention, p_roles, p_world, p_realm, action):
        """Check and Update warnme files"""
        
        if p_world not in self.worlds:
            return (False, "Unknown World. Must be haven or ra.")
        w_data = dataIO.load_json("data/regnum/warnme-" + p_world + ".json")
        
        exists = False
        
        if event == "invasion" or event == "wish":
            if not p_realm or p_realm not in self.realms:
                for role in p_roles:
                    if role.name.lower() in self.realms:
                        p_realm = role.name.lower()
                        break
                else:
                    return (False, "Your Realm is unknown or invalid.")
                
            if p_mention in w_data[event][p_realm]:
                exists = True
                
        elif event == "boss" or event == "tdm":
            if p_mention in w_data[event]:
                exists = True
        else:
            return (False, "Unknown event specified. It can only be : invasion, wish, boss or tdm.")
        
        if action == "add":
            if exists:
                return (False, "You're already set to receive notifications for this.")
            if event == "invasion" or event == "wish":
                w_data[event][p_realm].append(p_mention)
            else:
                w_data[event].append(p_mention)
        elif action == "del":
            if not exists:
                return (False, "You're already not getting notifications for this.")
            if event == "invasion" or event == "wish":
                w_data[event][p_realm].remove(p_mention)
            else:
                w_data[event].remove(p_mention)
        
        dataIO.save_json("data/regnum/warnme-" + p_world + ".json", w_data)
        
        return (True, p_realm)
    
    
    @commands.group(pass_context=True)
    @checks.is_owner()
    async def warnmeset(self, ctx):
        """Change settings on runtime"""
        if ctx.invoked_subcommand is None:
            await self.bot.send_cmd_help(ctx)
        
    
    @warnmeset.command(pass_context=True)
    @checks.is_owner()
    async def channel(self, ctx):
        """Set notifications to a channel or if not specified to PM"""
        self.output_channel = ctx.message.channel
        await self.bot.say("Warnme notifications will now be sent to this #{} channel.".format(ctx.message.channel.name))
        
    
    @warnmeset.command()
    @checks.is_owner()
    async def pm(self):
        """Set notifications to a channel or if not specified to PM"""
        self.output_channel = None
        await self.bot.say("Warnme notifications will now be sent via PM.")
            
            
    @warnmeset.command()
    @checks.is_owner()
    async def addid(self, id : str, world : str):
        """Add more Warstatus UIDs to watch"""
        if id not in self.warstatus_bot_ids:
            if world in self.worlds or world == "wishpin":
                self.warstatus_bot_ids[id] = world
                await self.bot.say("Added {} to the IDs to watch for status changes.".format(id))
            else:
                await self.bot.say("Invalid world")
        else:
            await self.bot.say("ID {} already is in the list.".format(id))
        
    
    @warnmeset.command()
    @checks.is_owner()
    async def delid(self, id : str):
        """Add more Warstatus UIDs to watch"""
        if id in self.warstatus_bot_ids:
            del self.warstatus_bot_ids[id]
            await self.bot.say("Removed {} from the IDs to watch for status changes.".format(id))
        else:
            await self.bot.say("ID {} is not in the list.".format(id))
    
            
    @commands.command(pass_context=True)
    async def warnme(self, ctx, event="invasion", world="haven", p_realm=""):
        """Receive a notification whenever your realm gets vulnerable, invaded, is about to make a wish, a boss respawns or a TDM session starts
        
        event can be: invasion boss wish tdm
        world can be: haven ra
        p_realm can be: alsius ignis syrtis (only required for invasion or wish)
        
        If called without arguments, will default to invasion haven and deduce your realm from your server roles.
        
        Examples :
        
        !warnme invasion haven syrtis
        !warnme boss
        !warnme wish ra ignis
        !warnme tdm ra"""

        try:
            role = ctx.message.author.roles
        except AttributeError:
            role = ()

        status = self.update(event.lower(), ctx.message.author.id, role, world.lower(), p_realm.lower(), "add")
        
        if not status[0]:
            message = status[1]
        else:
            message = "You'll get notified when "
            if event == "invasion":
                 message += "**" + status[1].capitalize() + " gets vulnerable or invaded!**"
            elif event == "wish":
                 message += "**" + status[1].capitalize() + " may be about to make a wish!**"
            elif event == "boss":
                 message += "**bosses respawn!**"
            elif event == "tdm":
                 message += "**TDM starts!**"
            message += " (" + world.capitalize() + ")"
            
        await self.bot.say("{}: {}".format(ctx.message.author.mention, message))
        
        
    @commands.command(pass_context=True)
    async def unwarnme(self, ctx, event="invasion", world="haven", p_realm=""):
        """Disable warnme notifications"""
        
        try:
            role = ctx.message.author.roles
        except AttributeError:
            role = ()
        
        status = self.update(event.lower(), ctx.message.author.id, role, world.lower(), p_realm.lower(), "del")
        
        if not status[0]:
            message = status[1]
        else:
            message = "You won't get notifications for "
            if event == "invasion":
                 message += "**" + status[1].capitalize() + " vulnerability/invasion**"
            elif event == "wish":
                 message += "**" + status[1].capitalize() + " wishes**"
            elif event == "boss":
                 message += "**bosses respawn**"
            elif event == "tdm":
                 message += "**TDM sessions**"
            message += " anymore. (" + world.capitalize() + ")"
            
        await self.bot.say("{}: {}".format(ctx.message.author.mention, message))

    
    async def notice_send(self, member_list, msg):
        if not self.output_channel:
            for member_id in member_list:
                member = await self.bot.get_user_info(member_id)
                await self.bot.send_message(member, ":bell: {}".format(msg))
        else:
            mention_list = ", ".join("<@{}>".format(member) for member in member_list)
            await self.bot.send_message(self.output_channel, "Hey {}! {}".format(mention_list,msg))
        
    
    async def wish_notice_timer(self, member_list, world, msg):
        await asyncio.sleep(500)
        await self.notice_send(member_list, msg)
        self.wish_timer[world] = None
    
    
    async def watch(self, message):
        """Watch war-status messages"""
        
        if message.author.id in self.warstatus_bot_ids.keys():
            
            message_l = message.content.lower()
            world = self.warstatus_bot_ids[message.author.id]
            
            #if "what do you have to say" in message_l:
            #    await self.bot.send_message(message.channel, "I couldn't care less :stuck_out_tongue_closed_eyes:")
            #    return
            
            if "wish" in message_l:
                
                for i in range(5):
                    try:
                        await self.bot.pin_message(message)
                    except discord.errors.HTTPException:
                        pins = await self.bot.pins_from(message.channel)
                        await self.bot.unpin_message(pins[-1])
                        continue
                    except discord.errors.Forbidden:
                        break
                    else:
                        break
                return
            
            if world == "wishpin":
                return
            
            w_data = dataIO.load_json(self.folder + "/warnme-" + world + ".json")
            member_list = []
            msg = None
            
            if "vulnerable" in message_l:
                
                for realm in self.realms:
                    if realm in message_l:
                        member_list = w_data["invasion"][realm]
                        break
                else:
                    return
                
                if not member_list:
                    return
                
                msg = "**{} Realm Gate is vulnerable!** ({})".format(realm.capitalize(), world.capitalize())
                    
            elif "invading" in message_l:
                
                for realm in self.realms:
                    if "invading {}".format(realm) in message_l:
                        member_list = w_data["invasion"][realm]
                        break
                else:
                    return
                
                if not member_list:
                    return
                
                msg = "**{} is being invaded!** ({})".format(realm.capitalize(), world.capitalize())
                
            elif "respawned" in message_l:
                
                for boss in self.bosses:
                    if boss in message_l:
                        member_list = w_data["boss"]
                        break
                else:
                    return
                
                if not member_list:
                    return
                
                msg = "**{} has respawned!** ({})".format(boss.capitalize(),world.capitalize())
                
            elif "a tdm" in message_l:
                
                member_list = w_data["tdm"]
                
                if not member_list:
                    return
                
                msg = "**A TDM session started!** ({})".format(world.capitalize())
                
            elif "gem" in message_l:
                
                if self.wish_timer[world]:
                    try:
                        self.wish_timer[world].cancel()
                    except:
                        pass
                    else:
                        self.wish_timer[world] = None
                else:
                    for trigger, realm in self.wish_triggers.items():
                        if trigger in message_l:
                            break
                    else:
                        return
                    
                    member_list = w_data["wish"][realm]
                    
                    if not member_list:
                        return
                    
                    msg = "**{} might be wishing soon!** ({})".format(realm.capitalize(),world.capitalize())
                
                    self.wish_timer[world] = self.loop.create_task(self.wish_notice_timer(member_list, world, msg))

                return
            
            if msg:
                await self.notice_send(member_list, msg)
                

def setup(bot):
    n = Warnme(bot)
    n.filecheck()
    bot.add_listener(n.watch, "on_message")
    bot.add_cog(n)
